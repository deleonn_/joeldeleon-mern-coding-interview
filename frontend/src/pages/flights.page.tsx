import React from "react";
import { RouteComponentProps } from "@reach/router";
import { Box, Typography } from "@material-ui/core";

import { FlightCard } from "../components/flights/flight-card";
import { FlightModel } from "../models/flight.model";

import { useFlights, useUpdateFlight } from "../hooks/flights.hook";

export const FlightsPage: React.FC<RouteComponentProps> = () => {
  const flights = useFlights();
  const mutateFlight = useUpdateFlight();

  const changeStatus = ({ status, _id }: Partial<FlightModel>) => {
    mutateFlight.mutate({ status, _id });
    // TODO: update frontend with mutation result
  };

  return (
    <Box>
      <Box>
        <Typography
          style={{ marginLeft: "15px", marginTop: "15px" }}
          variant="h4"
        >
          Scheduled Flights
        </Typography>
      </Box>

      <Box>
        {flights?.length
          ? flights.map((flight: FlightModel) => (
              <FlightCard
                _id={flight._id}
                origin={flight.origin}
                destination={flight.destination}
                code={flight.code}
                status={flight.status}
                onStatusChange={changeStatus}
              />
            ))
          : null}
      </Box>
    </Box>
  );
};

import { useQuery, useMutation } from "react-query";

import { BackendClient } from "../clients/backend.client";
import { FlightModel } from "../models/flight.model";

const backendClient = new BackendClient();

export interface MutationError {
  response: {
    data: {
      message: string;
    };
  };
}

export function useFlights() {
  const query = useQuery(["flights"], () => backendClient.getFlights());

  return query?.data?.data;
}

export function useUpdateFlight() {
  return useMutation((body: Partial<FlightModel>) =>
    backendClient.updateFlight(body)
  );
}

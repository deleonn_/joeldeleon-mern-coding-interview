import React, { FC } from "react";
import {
  Box,
  Card,
  Container,
  Typography,
  Select,
  MenuItem,
} from "@material-ui/core";

import { FlightModel, FlightStatuses } from "../../models/flight.model";

interface FlightCardProps {
  _id: string;
  code: string;
  origin: string;
  destination: string;
  passengers?: string[];
  status: FlightStatuses;
  onStatusChange: (value: Partial<FlightModel>) => void;
}

const mapFlightStatusToColor = (status: FlightStatuses) => {
  const mappings = {
    [FlightStatuses.Arrived]: "#1ac400",
    [FlightStatuses.Delayed]: "##c45800",
    [FlightStatuses["On Time"]]: "#1ac400",
    [FlightStatuses.Landing]: "#1ac400",
    [FlightStatuses.Cancelled]: "#ff2600",
  };

  return mappings[status] || "#000000";
};

export const FlightCard: React.FC<FlightCardProps> = (
  props: FlightCardProps
) => {
  const handleStatusUpdate = (
    event: React.ChangeEvent<{
      name?: string | undefined;
      value: unknown;
    }>
  ) => {
    props.onStatusChange({
      status: event.target.value as FlightStatuses,
      _id: props._id as string,
    });
  };

  const statuses = ["Arrived", "Landing", "On Time", "Delayed", "Cancelled"];

  return (
    <Card
      style={{
        backgroundColor: "#f5f5f5",
        margin: "15px",
        padding: "35px",
        justifyContent: "center",
      }}
    >
      <Box style={{ display: "flex", justifyContent: "space-between" }}>
        <Typography variant="h5">{props.code} </Typography>
        <Typography style={{ color: mapFlightStatusToColor(props.status) }}>
          Status: {props.status}
        </Typography>
        <Select
          value={props.status}
          label="Status"
          onChange={handleStatusUpdate}
        >
          {statuses.map((status) => (
            <MenuItem value={status} key={status}>
              {status}
            </MenuItem>
          ))}
        </Select>
      </Box>

      <Box>
        <Typography>Origin: {props.origin}</Typography>
      </Box>
      <Box>
        <Typography>Destination: {props.destination}</Typography>
      </Box>
    </Card>
  );
};

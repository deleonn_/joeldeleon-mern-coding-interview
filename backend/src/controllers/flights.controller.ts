import { JsonController, Get, Patch, Param, Body } from 'routing-controllers'
import { Flight } from '../models/flights.model'
import { FlightsService } from '../services/flights.service'

const flightsService = new FlightsService()

@JsonController('/flights', { transformResponse: false })
export default class FlightsController {
    @Get('')
    async getAll() {
        return {
            status: 200,
            data: await flightsService.getAll(),
        }
    }

    @Patch('/:id')
    async update(@Param('id') id: string, @Body() body: Flight) {
        return {
            status: 200,
            data: await flightsService.update(id, body),
        }
    }
}

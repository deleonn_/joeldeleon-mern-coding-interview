import { NotFoundError } from 'routing-controllers'
import { Flight, FlightsModel } from '../models/flights.model'

export class FlightsService {
    async getAll() {
        return await FlightsModel.find()
    }

    async update(id: string, body: Flight) {
        let response = await FlightsModel.findByIdAndUpdate(id, body, {
            new: true,
            runValidators: true,
        })

        if (!response) {
            throw new NotFoundError('error')
        }

        return response
    }
}
